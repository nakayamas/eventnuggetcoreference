package util

import java.io.{File, PrintWriter}

import util.FeatureMapper.{featureRegExp, saveString2File}

import scala.collection.mutable

/**
 * Created by nakayama.
 */
final class FeatureMapper(
                             featuresWithName: String
                             ) {

    private[this] val featureName2Index: mutable.Map[String, Int] = mutable.Map.empty[String, Int]

    // counter
    private[this] val count: Iterator[Int] = Stream.from(1).iterator

    val liblinearTrainFormatFeatures: String = featuresWithName.split("\n").map { line =>
        val features = line.split(" ")
        features.head + " " + features.tail.map {
            case featureRegExp(k, v) => {
                featureName2Index.get(k) match {
                    case Some(x) => x
                    case None => (featureName2Index += (k -> count.next))(k)
                }
            } + ":" + v
        }.sortBy { case featureRegExp(k, v) => k.toInt }.mkString(" ")
    }.mkString("\n")

    def saveAsLiblinearTrainFormat(file: File): Unit = saveString2File(liblinearTrainFormatFeatures, file)

    def saveMap(file: File): Unit =
        saveString2File(featureName2Index.map { case (k, v) => k + "\t" + v }.mkString("\n"), file)

}

object FeatureMapper {

    final private val featureRegExp = "(.+):([^:]+)".r

    final private def saveString2File(text: String, file: File) = {
        val writer = new PrintWriter(file)
        try {
            writer.write(text)
        } finally {
            writer.close()
        }
    }

}