package resolver

import java.io.{File, PrintWriter}

import de.bwaldvogel.liblinear._
import event.{EventHopper, EventHopperDoc, EventNugget, EventNuggetDoc}
import reader.{EventHopperFileReader, EventNuggetFileReader, TknFileReader}
import resolver.CoreferenceFeatureExtractor.Features
import tree.Tree
import util.{EventHopper2TBF, FeatureMapper}

import scala.collection.mutable.ArrayBuffer
import scala.io.Source

/**
 * Created by nakayama.
 */
object CoreferenceResolver extends App {

    final private[this] val (mode, cost, bias, modelFile, tknFiles, eventFiles, output) = parseArgs(args.toList)
    final private[this] val modelFilePrefix: String = (modelFile.getParent match {
        case null => ""
        case p => p + "/"
    }) + modelFile.getName + "_"
    final private[this] val featuresWithNameFile = new File(modelFilePrefix + "features_with_name")
    final private[this] val featuresFile = new File(modelFilePrefix + "features")
    final private[this] val featuresMapFile = new File(modelFilePrefix + "feature_indices")

    // main
    mode match {
        case 'train => train(eventFiles.map(EventHopperFileReader.parse))
        case 'test => test(eventFiles.map(EventNuggetFileReader.parse))
    }

    final private[this] def train(eventHopperDocs: Array[EventHopperDoc]) = {
        // parse tkn files
        val tknsByDoc = eventHopperDocs.map { doc =>
            val tkns = TknFileReader.parse(tknFiles.find(f => f.getName == doc.id + ".tab") match {
                case Some(x) => x
                case None => System.err.println(doc.id + ".tab is not found."); null
            })
            doc.eventNuggets.foreach(_.tkns = tkns)
            tkns
        }

        // feature extraction
        val extractor = new CoreferenceFeatureExtractor
        extractor.createFeaturesForTrain(eventHopperDocs)
        // output features with name
        val featuresWithName = extractor.liblinearTrainFormatWithFeatureName
        saveString2File(featuresWithName, featuresWithNameFile)

        // map feature name to number (Liblinear format)
        val featureMapper = new FeatureMapper(featuresWithName)
        featureMapper.saveAsLiblinearTrainFormat(featuresFile)
        featureMapper.saveMap(featuresMapFile)

        // train
        val parameter = new Parameter(SolverType.L2R_LR_DUAL, cost, Double.PositiveInfinity)
        val problem = Problem.readFromFile(featuresFile, bias)
        val model = Linear.train(problem, parameter)
        model.save(modelFile)
    }

    def debugPrint(s: String): Unit = {
        //        println(s)
    }

    final private[this] def test(eventNuggetDocs: Array[EventNuggetDoc]) = {
        val eventHopperDocs: ArrayBuffer[EventHopperDoc] = ArrayBuffer()

        // parse tkn files
        val tknsByDoc = eventNuggetDocs.map { doc =>
            val tkns = TknFileReader.parse(tknFiles.find(f => f.getName == doc.id + ".tab") match {
                case Some(x) => x
                case None => System.err.println(doc.id + ".tab is not found."); null
            })
            doc.eventNuggets.foreach(_.tkns = tkns)
            tkns
        }

        // read model file
        val model = Model.load(modelFile)

        // predict
        // resolve coreference per document
        eventNuggetDocs.foreach { eventNuggetDoc =>
            val extractor = new CoreferenceFeatureExtractor
            Source.fromFile(featuresMapFile).getLines.foreach { l =>
                val line = l.split("\t")
                extractor.featureName2Index += (line(0) -> line(1).toInt)
            }

            def predictProbability(features: Features): Double = {
                val probabilityResults = new Array[Double](2)
                Linear.predictProbability(model, extractor.liblinearPredictFormat(features), probabilityResults)
                // Class Number: Positive... probabilityResults(0), Negative... probabilityResults(1)
                probabilityResults(0)
            }

            // predict and create a tree
            val tree = new Tree
            val eventNuggets = eventNuggetDoc.eventNuggets
            eventNuggets.indices.foreach { n =>
                val i: EventNugget = eventNuggets(n)
                debugPrint("-----about " + i + "-------")
                // calculate coreference probabilities, and then make a tree grow
                var mostProbableEventNugget: (EventNugget, Double) = (null, predictProbability(extractor.extractFeatures(i)))
                debugPrint("    p: " + mostProbableEventNugget._2)
                for {
                    j: EventNugget <- eventNuggets.take(n) if i.realis == j.realis
                } {
                    debugPrint("  take " + j)
                    val probability = predictProbability(extractor.extractFeatures(i, j))
                    debugPrint("    i, j p: " + probability)
                    debugPrint("    most p: " + mostProbableEventNugget)
                    if (mostProbableEventNugget._2 < probability) {
                        mostProbableEventNugget = (j, probability)
                        debugPrint("    !!! p is updated !!!")
                    }
                }
                if (mostProbableEventNugget._1 == null) {
                    // create a new cluster
                    debugPrint(" >> new cluster! " + i)
                    tree.add(i)
                } else {
                    // append to an exist node
                    tree.nodes.find(_.eventNugget == mostProbableEventNugget._1).get.add(i)
                    debugPrint(" >> under " + mostProbableEventNugget._1)
                }
                debugPrint("")
            }

            // convert a tree to clusters
            val eventHopperId: Iterator[Int] = Stream.from(1).iterator
            eventHopperDocs +=
                new EventHopperDoc(eventNuggetDoc.id, eventNuggetDoc.sourceType,
                    // create clusters
                    tree.nodes.filter(_.isClusterTop).map(_.subNodes.map(_.eventNugget)).map {
                        new EventHopper("h-" + eventHopperId.next, _)
                    })
        }

        // output as tbf
        print(EventHopper2TBF(eventHopperDocs))
    }

    final private[this] def saveString2File(text: String, file: File) = {
        val writer = new PrintWriter(file)
        try {
            writer.write(text)
        } finally {
            writer.close()
        }
    }

    final private def parseArgs(args: List[String]) = {
        // initial check of options
        if (args.contains("-h")) showHelpAndExit(0)
        if (args.contains("--train") && args.contains("--test")) showHelpAndExit(-1, "Specify --train or --test but not both.")

        // default option values
        type TRAIN_OR_TEST = Symbol
        var mode: TRAIN_OR_TEST = null
        var cost: Double = 0.1
        var bias: Int = 1
        var model: File = null
        var files: Array[File] = null
        var tknFiles: Array[File] = null
        var output: File = null

        @scala.annotation.tailrec
        def parseArgsHelper(args: List[String]): (TRAIN_OR_TEST, Double, Int, File, Array[File], Array[File], File) = {
            args match {
                case "--train" :: rest => {
                    if (mode != null) showHelpAndExit(-1, "Specify --train or --test but not both.")
                    mode = 'train
                    parseArgsHelper(rest)
                }
                case "--test" :: rest => {
                    if (mode != null) showHelpAndExit(-1, "Specify --train or --test but not both.")
                    mode = 'test
                    parseArgsHelper(rest)
                }
                case "-c" :: c :: rest => {
                    try {
                        cost = c.toDouble
                    } catch {
                        case e: java.lang.NumberFormatException => showHelpAndExit(-1, "Cost parameter must be a real number.")
                    }
                    parseArgsHelper(rest)
                }
                case "-b" :: b :: rest => {
                    try {
                        bias = b.toInt
                    } catch {
                        case e: java.lang.NumberFormatException => showHelpAndExit(-1, "Bias parameter must be an integer.")
                    }
                    parseArgsHelper(rest)
                }
                case "-t" :: t :: rest => {
                    try {
                        tknFiles = new File(t).listFiles
                    }
                    catch {
                        case e: java.io.FileNotFoundException => showHelpAndExit(-1, rest(1) + ": No such file or directory"); null
                    }
                    parseArgsHelper(rest)
                }
                case "-m" :: m :: rest => {
                    model = new File(m)
                    parseArgsHelper(rest)
                }
                case "-o" :: o :: rest => {
                    try {
                        output = new File(o)
                    }
                    catch {
                        case e: java.io.FileNotFoundException => showHelpAndExit(-1, o + ": No such file or directory")
                    }
                    parseArgsHelper(rest)
                }
                case rest => {
                    files = rest.flatMap { name =>
                        val f =
                            try {
                                new File(name)
                            }
                            catch {
                                case e: java.io.FileNotFoundException => showHelpAndExit(-1, name + ": No such file or directory"); null
                            }
                        f.isDirectory match {
                            case true => f.listFiles
                            case false => Array(f)
                        }
                    }.toArray
                    (mode, cost, bias, model, tknFiles, files, output)
                }
            }
        }

        parseArgsHelper(args)
    }

    final private def showHelpAndExit(exitStatus: Int, message: String = null) {
        if (message != null) Console.err.println(message)
        val help = """Usage: CoreferenceResolver (--train -c cost -b bias|--test) -m model_file -t tkn_dir [-o output_file] (<event_nugget_dir>|<event_nugget_file>...)"""
        Console.err.println(help)
        sys.exit(exitStatus)
    }

}
