package reader

import java.io.File

import event._

import scala.xml.XML

/**
 * Created by nakayama.
 */
object EventHopperFileReader {

    def parse(enFile: File): EventHopperDoc = parse(XML.loadFile(enFile))

    def parse[EventType, EventSubtype, Realis](enx: xml.Elem): EventHopperDoc = {
        // <root>
        val infoNode = enx \\ "deft_ere_event_nuggets"
        val docId = (enx \\ "deft_ere_event_nuggets" \ "@doc_id").text
        val sourceType = Symbol((enx \\ "deft_ere_event_nuggets" \ "@source_type").text)
        // <hopper>
        val eventHoppers = (enx \\ "hopper").map { eh =>
            val eventHopperId = (eh \ "@id").text
            // <event_mention>
            val eventNuggets = (eh \\ "event_mention").map { em =>
                val eventNuggetId = (em \ "@id").text
                val eventType = Symbol((em \ "@type").text)
                val eventSubtype = Symbol((em \ "@subtype").text)
                val realis = Realis.withName((em \ "@realis").text.toUpperCase)
                val formality = Symbol((em \ "@formality").text)
                val schedule = Symbol((em \ "@schedule").text)
                val medium = Symbol((em \ "@medium").text)
                val audience = Symbol((em \ "@audience").text)
                val trigger = em \ "trigger"
                val offset = (trigger \ "@offset").text.toInt
                val length = (trigger \ "@length").text.toInt
                val text = trigger.text
                new EventNugget(eventNuggetId, eventType, eventSubtype, realis, formality, schedule, medium, audience, offset, length, text)
            }.sortBy(_.startOffset)
            new EventHopper(eventHopperId, eventNuggets)
        }
        new EventHopperDoc(docId, sourceType, eventHoppers)
    }

}
