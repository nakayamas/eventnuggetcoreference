package event

/**
 * Created by nakayama.
 */
final case class EventHopperDoc(id: String,
                                source_type: Symbol,
                                var eventHoppers: Seq[EventHopper])
    extends EventDoc(id, source_type) {

    var eventNuggets: Seq[EventNugget] = eventHoppers.flatMap(_.eventNuggets).sortBy(_.startOffset)

    eventNuggets.foreach(_.eventDoc = this)

    eventHoppers = eventHoppers.sortBy(_.eventNuggets.head.startOffset)

}