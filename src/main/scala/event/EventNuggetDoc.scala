package event

/**
 * Created by nakayama.
 */
final case class EventNuggetDoc(id: String,
                          sourceType: Symbol,
                          var eventNuggets: Seq[EventNugget])
    extends EventDoc(id, sourceType) {

    eventNuggets.foreach(_.eventDoc = this)

    eventNuggets = eventNuggets.sortBy(_.startOffset)

}
