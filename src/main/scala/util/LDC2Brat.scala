package util

import java.io.{File, PrintWriter}

import reader.EventHopperFileReader

/**
 * Created by nakayama.
 */
object LDC2Brat extends App {

    require(args.length >= 3 && args.head == "-o",
        """Usage: LDC2Brat -o output_dir (ldc_xml_file|ldc_xml_dir)""")

    // main
    val outDir = args(1)
    args.drop(2).map(new File(_)).flatMap { f =>
        f.isDirectory match {
            case true => f.listFiles
            case false => Array(f)
        }
    }.foreach { f =>
        val enIndex = new Counter(1)
        val realisIndex = new Counter(1)
        val corefIndex = new Counter(1)
        val output = new StringBuilder
        EventHopperFileReader.parse(f).eventHoppers.foreach { hoppers =>
            hoppers.eventNuggets.foreach { en =>
                output.append(s"T${enIndex.next}\t${en.eventTypeExp} ${en.startOffset} ${en.endOffset}\t${en.text}\n")
                output.append(s"${en.tbfId}\t${en.eventTypeExp}:T${enIndex.current}\n")
                output.append(s"A${realisIndex.next}\tRealis ${en.tbfId} ${en.realisExp}\n")
            }
            hoppers.eventNuggets.sliding(2).toIndexedSeq.foreach { corefArgs =>
                output.append(s"R${corefIndex.next}\tCoreference " +
                    (corefArgs match {
                        case List(arg1, arg2) => s"Arg1:${arg1} Arg2:${arg2}\n"
                        case List(arg1) => s"Arg1:${arg1}\n"
                    }) + "\n")
            }
        }

        val file = new File(outDir + "/" + f.getName.dropRight(4) + ".ann")
        val writer = new PrintWriter(file)
        try {
            writer.write(output.result)
        } finally {
            writer.close()
        }
    }

    final private[this] class Counter(
                                         var n: Int
                                         ) {
        def next: Int = {
            n += 1
            n
        }

        def current = n

    }

}
