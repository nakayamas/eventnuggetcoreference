package event

/**
 * Created by nakayama.
 */
case class Tkn(
                  tknIndex: Int,
                  text: String,
                  startOffset: Int,
                  endOffset: Int
                  ) {

    private var _eventNugget: EventNugget = _

    def eventNugget: EventNugget = _eventNugget

    def eventNugget_=(eventNugget: EventNugget) = _eventNugget = eventNugget

    var lemma: String = null
    var pos: String = null
    var pos2: String = null
    var ne: String = null

}
