package util

import java.io.{File, PrintWriter}

import scala.io.Source

/**
 * Created by nakayama.
 */
object Text2Tkn extends App {

    require(args.length >= 3 && args.head == "-o",
        """Usage: Text2Tkn -o output_dir (text_file|text_dir)""")

    // main
    val outDir = args(1)
    args.drop(2).map(new File(_)).flatMap { f =>
        f.isDirectory match {
            case true => f.listFiles
            case false => Array(f)
        }
    }.foreach { f =>
        var offset = 0
        val index = Stream.from(1).iterator
        val output =
            Source.fromFile(f).getLines().map { l =>
                l.split(" ").map { w =>
                    val currentOffset = offset
                    offset += w.length + 1 // for space, + 1
                    Seq(index.next, w, currentOffset, offset - 1).mkString("\t")
                }.mkString("\n")
            }.mkString("\n")

        val file = new File(outDir + "/" + f.getName.dropRight(4) + ".tab")
        val writer = new PrintWriter(file)
        try {
            writer.write(output)
        } finally {
            writer.close()
        }
    }

}
