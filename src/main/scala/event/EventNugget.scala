package event

import event.EventNugget.upperFirstLowerRest

/**
 * Created by nakayama.
 */
final case class EventNugget(
                                id: String,
                                eventType: Symbol,
                                eventSubtype: Symbol,
                                realis: Realis.Value,
                                formality: Symbol,
                                schedule: Symbol,
                                medium: Symbol,
                                audience: Symbol,
                                startOffset: Int,
                                length: Int,
                                text: String) {

    val words: Array[String] = text.split(" ")

    val endOffset: Int = startOffset + length

    val startOffsets: Array[Int] = {
        var os = startOffset
        words.map { w =>
            val currentOffset = os
            os += w.length + 1 // word length + space
            currentOffset
        }
    }

    private var _eventHopper: Option[EventHopper] = None

    def eventHopper: Option[EventHopper] = _eventHopper

    def eventHopper_=(eventHopper: Option[EventHopper]) = _eventHopper = eventHopper

    private var _eventDoc: EventDoc = _

    def eventDoc: EventDoc = _eventDoc

    def eventDoc_=(eventDoc: EventDoc) = _eventDoc = eventDoc

    private var _tkns: Seq[Tkn] = _

    def tkns: Seq[Tkn] = _tkns

    def tkns_=(tkns: Seq[Tkn]) = _tkns = tkns

    lazy val tkn: Option[Tkn] = tkns.find(_.startOffset == startOffset)

    val tbfId = id.replace("em-", "E")

    lazy val tknIndices: Array[Int] = startOffsets.map(os => tkns.find(_.startOffset == os).get.tknIndex)

    val eventTypeExp = upperFirstLowerRest(eventType.name) + "_" + upperFirstLowerRest(eventSubtype.name)

    val realisExp = upperFirstLowerRest(realis.toString)

}

object EventNugget {

    final private def upperFirstLowerRest(str: String) = str.head.toUpper + str.drop(1).toLowerCase

}