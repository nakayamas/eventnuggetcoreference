package util

import java.util
import java.util.Properties

import edu.stanford.nlp.ling.CoreAnnotations._
import edu.stanford.nlp.ling.CoreLabel
import edu.stanford.nlp.pipeline.{Annotation, StanfordCoreNLP}

import scala.collection.JavaConversions._

/**
 * Created by nakayama.
 */
final class CoreNLP(
                       coreNLP: StanfordCoreNLP,
                       text: String
                       ) {

    private[this] val annotation = new Annotation(text)
    coreNLP.annotate(annotation)

    private[this] val labels: util.List[CoreLabel] = annotation.get(classOf[TokensAnnotation])

    final case class TknWithCoreNLP(label: CoreLabel) {
        val startOffset: Integer = label.get(classOf[CharacterOffsetBeginAnnotation])
        val endOffset: Integer = label.get(classOf[CharacterOffsetEndAnnotation])
        val surface: String = label.get(classOf[OriginalTextAnnotation])
        val lemma: String = label.get(classOf[LemmaAnnotation])
        val pos: String = label.get(classOf[PartOfSpeechAnnotation])
//        val ne: String = label.get(classOf[NamedEntityTagAnnotation])
    }

    val tknsWithCoreNLP: Map[Integer, TknWithCoreNLP] = labels.map { l =>
        val t = TknWithCoreNLP(l)
        (t.startOffset, t)
    }.toMap

}

object CoreNLP {

    final private[this] val properties: Properties = new Properties()
    properties.setProperty("annotators", "tokenize, ssplit, pos, lemma")
    final private[this] val coreNLP: StanfordCoreNLP = new StanfordCoreNLP(properties)

    def apply(text: String): CoreNLP = new CoreNLP(coreNLP, text)

}
