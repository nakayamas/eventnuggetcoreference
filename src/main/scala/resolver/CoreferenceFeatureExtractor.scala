package resolver

import de.bwaldvogel.liblinear.{Feature, FeatureNode}
import event.{EventHopperDoc, EventNugget, EventNuggetDoc}
import resolver.CoreferenceFeatureExtractor.{Features, NEGATIVE, PN, POSITIVE, featureValueConverter}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
 * Created by nakayama.
 */
final class CoreferenceFeatureExtractor {

    private[this] var samples: ArrayBuffer[Features] = ArrayBuffer()

    private[this] var usedAllFeatures: ArrayBuffer[String] = ArrayBuffer()

    def createFeaturesForTrain(eventHopperDocs: Array[EventHopperDoc]): Unit =
        eventHopperDocs.foreach(createFeaturesForTrain)

    def createFeaturesForTrain(eventHopperDoc: EventHopperDoc): Unit =
        createFeaturesForTrain(eventHopperDoc.eventNuggets)

    def createFeaturesForTrain(eventNuggets: Seq[EventNugget]): Unit =
        eventNuggets.indices.foreach { n =>
            val i: EventNugget = eventNuggets(n)
            val posNegValue = i == i.eventHopper.get.eventNuggets.head match {
                case true => POSITIVE
                case false => NEGATIVE
            }
            samples += extractFeatures(i) + (PN -> posNegValue)
            for {
                j <- eventNuggets.take(n) if i.realis == j.realis
            } {
                val posNegValue = i.eventHopper.get == j.eventHopper.get match {
                    case true => POSITIVE
                    case false => NEGATIVE
                }
                samples += extractFeatures(i, j) + (PN -> posNegValue)
            }
        }

    def createFeaturesForPredict(eventNuggetDoc: EventNuggetDoc): Unit =
        createFeaturesForPredict(eventNuggetDoc.eventNuggets)

    def createFeaturesForPredict(eventNuggets: Seq[EventNugget]): Unit =
        eventNuggets.indices.foreach { n =>
            val i: EventNugget = eventNuggets(n)
            samples += extractFeatures(i)
            eventNuggets.take(n).foreach { j: EventNugget =>
                samples += extractFeatures(i, j)
            }
        }

    /**
     * whether i creates a new cluster
     *
     * @param i focused mention
     * @return
     */
    def extractFeatures(i: EventNugget): Features = {
        val features: Features = mutable.HashMap.empty[String, Any]

        addFeature(features, "unseen_word_surface",
            i.eventDoc.eventNuggets.takeWhile(_ != i).exists(_.text == i.text))
        addFeature(features, "surface_" + i.text.replace(" ", "_"), 1)

        features
    }

    def extractFeatures(i: EventNugget, j: EventNugget): Features = {
        val features: Features = mutable.HashMap.empty[String, Any]

        addFeature(features, "same_surface", i.text == j.text)
        addFeature(features, "same_realis", i.realis == j.realis)
        addFeature(features, "same_type", i.eventType == j.eventType)
        addFeature(features, "same_subtype", i.eventSubtype == j.eventSubtype)
        addFeature(features, "same_type_subtype", i.eventType == j.eventType && i.eventSubtype == j.eventSubtype)
        // tkn file と stanford corenlp の tokenizer の違いで lemma や pos などが null の場合がある
//        addFeature(features, "same_lemma", i.tkn.get.lemma != null && i.tkn.get.lemma == j.tkn.get.lemma)
//        addFeature(features, "same_pos", i.tkn.get.pos != null && i.tkn.get.pos == j.tkn.get.pos)

        features
    }

    private[this] def addFeature(f: Features, k: String, v: Any): Unit = {
        f.put(k, v)
        if (!usedAllFeatures.contains(k)) usedAllFeatures += k
    }

    def liblinearPredictFormat(features: Features): Array[Feature] =
        features.keys.filter { k =>
            featureName2Index.get(k) match {
                case Some(x) => true
                case None => false
            }
        }.map { k =>
            new FeatureNode(featureName2Index(k), featureValueConverter(features(k)))
        }.toArray

    private var _featureName2Index: mutable.Map[String, Int] = mutable.Map.empty[String, Int]

    def featureName2Index: mutable.Map[String, Int] = _featureName2Index

    def featureName2Index_=(featureName2Index: mutable.Map[String, Int]) = _featureName2Index = featureName2Index

    def featureIndexMapString: String = usedAllFeatures.indices.map(i => usedAllFeatures(i) + "\t" + (i + 1)).mkString("\n")

    def liblinearTrainFormatWithFeatureName: String =
        samples.map { s =>
            s(PN) + " " + s.keys.collect {
                case k if k != PN => k + ":" + featureValueConverter(s(k))
            }.mkString(" ")
        }.mkString("\n")

}

object CoreferenceFeatureExtractor {

    final type Features = mutable.Map[String, Any]

    final val PN: String = "PosNeg"
    final val POSITIVE: Int = 1
    final val NEGATIVE: Int = 2

    final private def bool2Int(b: Boolean): Int = b match {
        case true => 1
        case false => 0
    }

    final private def featureValueConverter(v: Any): Double =
        v match {
            case i: Int => i
            case d: Double => d
            case b: Boolean => bool2Int(b)
            case null => 0.0
        }

}
