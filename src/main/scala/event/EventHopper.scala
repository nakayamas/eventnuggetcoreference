package event

/**
 * Created by nakayama.
 */
final case class EventHopper(
                                id: String,
                                eventNuggets: Seq[EventNugget]) {

    eventNuggets.foreach(_.eventHopper = Some(this))

    val tbfId = id.replace("h-", "C")

}
