package util

import java.io.{File, PrintWriter}

import event.{EventHopperDoc, EventNugget}

import scala.collection.mutable.ArrayBuffer

/**
 * Created by nakayama.
 */
final class EventHopper2TBF(
                               eventHopperDocs: Seq[EventHopperDoc]
                               ) {

    def this(eventHopperDoc: EventHopperDoc) = this(Seq(eventHopperDoc))

    override def toString: String = {
        val tbf = new StringBuilder()
        eventHopperDocs.foreach { doc =>
            tbf.append("#BeginOfDocument " + doc.id + "\n")
            doc.eventHoppers.foreach { hopper =>
                val ens = hopper.eventNuggets
                val singleTagEN: ArrayBuffer[EventNugget] = ArrayBuffer()
                ens.foreach { en =>
                    // skip double tagged event mention
                    singleTagEN.find(_.startOffset == en.startOffset) match {
                        case Some(x) =>
                        case None => {
                            // event nuggets
                            tbf.append(Seq("system1", doc.id, en.tbfId, en.tknIndices.mkString(","), en.text, en.eventTypeExp, en.realisExp).mkString("\t"))
                            tbf.append("\n")
                            singleTagEN += en
                        }
                    }
                }
                // coreferences
                tbf.append(Seq("@Coreference", hopper.tbfId, singleTagEN.map(_.tbfId).mkString(",")).mkString("\t"))
                tbf.append("\n")
            }
            tbf.append("#EndOfDocument\n")
        }
        tbf.result()
    }

    def save(file: File): Unit = {
        val writer = new PrintWriter(file)
        try {
            writer.write(this.toString)
        } finally {
            writer.close()
        }
    }

}

object EventHopper2TBF {

    final def apply(eventHopperDocs: Seq[EventHopperDoc]) = new EventHopper2TBF(eventHopperDocs)

    final def apply(eventHopperDoc: EventHopperDoc) = new EventHopper2TBF(eventHopperDoc)

}