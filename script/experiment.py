#!/usr/bin/env python
# encoding: utf-8

import subprocess, shutil, os, sys

script_dir = os.path.dirname(__file__)

def cmd(args, out=sys.stdout):
    print "\"" + reduce(lambda x, y: str(x) + "\" \"" + str(y), args) + "\""

    result = ""
    try:
        result = subprocess.check_output(args)
    except subprocess.CalledProcessError as e:
        sys.stderr.write(str(e) + "\n")
        sys.exit(2)

    if out != sys.stdout:
        out = open(out, "w")
    out.write(result)
    out.flush()

    return result

def conversion():
    data_dir = script_dir + "/../data/train_data"
    ldc_text_dir = data_dir + "/source"
    ldc_ann_dir = data_dir + "/event_hopper"
    brat_output_dir = data_dir + "/ann"
    token_table_dir = data_dir + "/tkn"
    output_tbf_basename = data_dir + "/tbf/gold"

    cmd(["java", "-jar", script_dir + "/../EvmEval/bin/converter-1.0.3-jar-with-dependencies.jar",
         "-t", ldc_text_dir,
         "-te", "txt",
         "-a", ldc_ann_dir,
         "-ae", "event_hoppers.xml",
         "-o", brat_output_dir])

    cmd(["java", "-jar", script_dir + "/../EvmEval/bin/token-file-maker-1.0.4-jar-with-dependencies.jar",
         "-a", brat_output_dir,
         "-t", brat_output_dir,
         "-e", "txt",
         "-o", token_table_dir])

    cmd(["python", script_dir + "/../EvmEval/brat2tbf.py",
         "-t", token_table_dir,
         "-d", brat_output_dir,
         "-o", output_tbf_basename,
         "-w"])

def n_fold(n):
    import shutil

    data_dir = script_dir + "/../data/train_data"
    event_hopper_dir = data_dir + "/event_hopper"
    event_nugget_dir = data_dir + "/event_nugget"
    # extract only filenames
    org_files = map(lambda f: ".".join(f.split(".")[:-2]), os.listdir(event_hopper_dir))

    for i in xrange(n):
        maked_dir = event_hopper_dir + "_" + str(i)
        if not os.path.exists(maked_dir):
            os.mkdir(maked_dir)
        maked_dir = event_nugget_dir + "_" + str(i)
        if not os.path.exists(maked_dir):
            os.mkdir(maked_dir)

    for i in xrange(len(org_files)):
        shutil.copyfile(event_hopper_dir + "/" + org_files[i] + ".event_hoppers.xml",
                        event_hopper_dir + "_" + str(i % n) + "/" + org_files[i] + ".event_hoppers.xml")
        shutil.copyfile(event_nugget_dir + "/" + org_files[i] + ".event_nuggets.xml",
                        event_nugget_dir + "_" + str(i % n) + "/" + org_files[i] + ".event_nuggets.xml")

def dict2str(d):
    return "_".join(map(lambda p: p[0] + str(p[1]), d.items()))

def exp(train_data_list, test_data_list, prms):
    train_data_dir = script_dir + "/../data/train_data"
    test_data_dir = script_dir + "/../data/test_data"
    # for cross validation
    if any("train_data" in e for e in test_data_list):
        test_data_dir = train_data_dir
    result_dir = "result_" + dict2str(prms)
    if not os.path.exists(result_dir):
        os.mkdir(result_dir)

    home = subprocess.check_output("/bin/echo -n ${HOME}", shell=True)
    cp = [home + "/.ivy2/cache/de.bwaldvogel/liblinear/jars/liblinear-1.95.jar",
        home + "/.ivy2/cache/edu.stanford.nlp/stanford-corenlp/jars/stanford-corenlp-3.5.2.jar",
        home + "/.ivy2/cache/edu.stanford.nlp/stanford-corenlp/jars/stanford-corenlp-3.5.2-models.jar",
        script_dir + "/../target/scala-2.11/classes"]

    # Train
    cmd(["scala", "-J-Xmx10G", "-cp", ":".join(cp), "resolver.CoreferenceResolver", "--train",
        "-c", prms["c"],
        "-b", prms["b"],
        "-t", train_data_dir + "/tkn",
        "-m", result_dir + "/model"] +
        train_data_list,
        out=result_dir + "/train_dump")

    # Test
    cmd(["scala", "-J-Xmx10G", "-cp", ":".join(cp), "resolver.CoreferenceResolver", "--test",
        "-t", test_data_dir + "/tkn",
        "-m", result_dir + "/model"] +
        test_data_list,
        out=result_dir + "/system.tbf")

    # Eval
    result = cmd(["python", script_dir + "/../EvmEval/scorer_v1.6.py",
        "-g", test_data_dir + "/tbf/gold.tbf",
        "-s", result_dir + "/system.tbf",
        "-d", result_dir + "/comparison",
        "-t", test_data_dir + "/tkn",
        "-c", result_dir + "/conll-coref"],
        out=result_dir + "/result")

    import re
    return re.search(r"Overall Average CoNLL score : (.+)", result).group(1)

def real_test():
    train_data_list = [ script_dir + "/../data/train_data/event_hopper" ]
    test_data_list = [ script_dir + "/../data/test_data/nugget" ]
    p = { "b": "-1", "c": "0.01" }
    result = exp(train_data_list, test_data_list, p)
    print result
    with open("result", "w") as f:
        f.write(result)

def cross_valid(n, prms):
    results = []
    for i in reversed(range(n)):
        train_data_list = [ script_dir + "/../data/train_data/event_hopper_" + str(y) for y in filter(lambda x: x != i, xrange(n)) ]
        test_data_list = [ script_dir + "/../data/train_data/event_nugget_" + str(i) ]
        prms["n"] = i
        results.append(float(exp(train_data_list, test_data_list, prms)))
    return sum(results) / len(results)

def prm_tune():
    prms = {
        "b": [-1], # use bias or not
        "c": [0.01] # cost parameter
    }

    output = ""
    best = ("init", 0.0)
    for b in prms["b"]:
        for c in prms["c"]:
            p = { "b": str(b), "c": str(c) }
            result = cross_valid(5, p)
            output += dict2str(p) + "\t" + str(result) + "\n"
            if best[1] < result:
                best = (dict2str(p), result)

    print output
    with open("summary", "w") as f:
        f.write(output)
        f.write("best: " + str(best))

    print output
    print "best: " + str(best)

def visualise(data_dir, result_dir, output_dir):
    # cross docement で system が document を吐かない場合に comparison が - となるので、それを削除する
    comparison_file = result_dir + "/comparison"
    comparison = ""
    exist_system_output = True
    document = ""
    for l in open(comparison_file, "r"):
        # unknown bug of scorer_v1.6.py
        l = l.replace(",,", ",")
        l = l.replace(",\n", "\n")

        document += l
        if l.endswith("|\t-\t-\n"):
            exist_system_output = False
        elif l.startswith("#EndOfDocument"):
            if exist_system_output:
                comparison += document
            document = ""
            exist_system_output = True
    with open(comparison_file, "w") as file:
        file.write(comparison)

    visualization_dir = result_dir + "/visualization"
    if not os.path.exists(visualization_dir):
        shutil.copytree(script_dir + "/../EvmEval/visualization", visualization_dir)
    cmd(["python", script_dir + "/../EvmEval/visualization_tools/visualize.py",
        "-d", comparison_file,
        "-t", data_dir + "/tkn",
        "-x", data_dir + "/source",
        "-v", visualization_dir,
        "-wos"],
        out=result_dir + "/visualization_result")
    if os.path.exists(output_dir):
        shutil.rmtree(output_dir)
    shutil.copytree(visualization_dir, output_dir)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        sys.stderr.write("Usage: ./" + os.path.basename(__file__) + " (conversion|n_fold|cross_valid|prm_tune|real_test|(visualise <data_dir> <result_dir> <output_dir>))\n")
        sys.exit(2)

    subcommand = sys.argv[1]
    if subcommand == "conversion":
        conversion()
    elif subcommand == "n_fold":
        n_fold(5)
    elif subcommand == "cross_valid":
        p = { "b": "-1", "c": "0.01" }
        result = str(cross_valid(5, p))
        print result
        with open("result", "w") as f:
            f.write(result)
    elif subcommand == "prm_tune":
        prm_tune()
    elif subcommand == "real_test":
        real_test()
    elif subcommand == "visualise":
        visualise(sys.argv[2], sys.argv[3], sys.argv[4])