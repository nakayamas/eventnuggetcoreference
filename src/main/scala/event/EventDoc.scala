package event

/**
 * Created by nakayama.
 */
abstract class EventDoc(id: String,
                        sourceType: Symbol
                           ) {

    var eventNuggets: Seq[EventNugget]

}