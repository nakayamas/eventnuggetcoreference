name := "EventNuggetCoreference"

version := "1.0"

scalaVersion := "2.11.7"

scalacOptions += "-deprecation"

libraryDependencies ++= Seq(
    "org.scala-lang.modules" %% "scala-xml" % "1.0.5",
    "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test",
    "de.bwaldvogel" % "liblinear" % "1.95",
    "edu.stanford.nlp" % "stanford-corenlp" % "3.5.2",
    "edu.stanford.nlp" % "stanford-corenlp" % "3.5.2" classifier "models"
)