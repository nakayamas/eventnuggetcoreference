package reader

import java.io.File

import event.Tkn
import util.CoreNLP

import scala.io.Source

/**
 * Created by nakayama.
 */
object TknFileReader {

    def parse(file: File): Seq[Tkn] = {
        // Read tkn file
        val tkns: Seq[Tkn] = Source.fromFile(file).getLines().map {
            _.split("\t") match {
                case Array(i, t, startOffset, endOffset) => new Tkn(i.toInt, t, startOffset.toInt, endOffset.toInt)
            }
        }.toIndexedSeq
        val text: StringBuilder = new StringBuilder
        tkns.foreach { tkn =>
            // fill spaces
            text.append(Range(0, tkn.startOffset - text.length).map(_ => ' ').mkString + tkn.text)
        }

        // Parse text by Stanford CoreNLP
        val coreNLP: CoreNLP = CoreNLP(text.result)
        tkns.foreach { tkn =>
            coreNLP.tknsWithCoreNLP.get(tkn.startOffset) match {
                case Some(label) => {
                    tkn.lemma = label.lemma
                    tkn.pos = label.pos
                    tkn.pos2 = label.pos.take(2)
                    //            tkn.ne = label.ne
                }
                case None =>
            }
        }

        // TODO: SRL by ?

        tkns
    }

}